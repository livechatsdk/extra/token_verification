# Token Verification



## Getting started

* Add jwttokenserver.jar file to your project
* Implement the abstract class 'TokenProvider'
* Build a new jar file & deploy it proxy server note the path ( pathA )
* Add below keys to tcomm proxy config file at : Tomcat\conf\configs\app.config
    *    ICP_ENABLE_CUSTOM_TOKEN_VALIDATION=true
    *    ICP_CUSTOM_TOKEN_VALIDATION_CLASSPATH=<path A>

* Restart tomcat
