package com.tetherfi.cp.token.validation;

import com.tetherfi.token.TokenProvider;

import java.util.HashMap;
import java.util.Map;

/**
 * @author SujanChatra
 */

public class CustomTokenValidation extends TokenProvider {

    @Override
    public String generateToken(Map<String, Object> userData) {
        //TODO : No need to implement this method
        return null;
    }

    /**
     * @param authToken - contains Auth Token
     * @param expCheck - to check the token expiry date and return false if token is expired
     * @return returns true or false
     */

    @Override
    public boolean verifyToken(String authToken, boolean expCheck) {
        //TODO : Implement your token validation logic
        //TODO : Perform token expiry check if 'expCheck' is true
        return true;
    }

    /**
     *
     * @param authToken - contains Token
     * @return returns Token Body or the user data
     */
    @Override
    public Map<String, Object> getTokenBody(String authToken) {
        //TODO : Return empty map or UserData
        Map<String, Object> tokenBody = new HashMap<String, Object>();
        return tokenBody;
    }
}
